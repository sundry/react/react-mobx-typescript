import React from 'react';
import Header from 'components/Header/Header';
import Input from 'components/Input/Input';

const App: React.FC = () => {
  return (
    <div>
      <Header />
      <Input />
    </div>
  );
}

export default App;
