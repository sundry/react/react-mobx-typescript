import React from 'react';
import UserStore from 'stores/UserStore';
import { observer } from 'mobx-react';

const Header: React.FC = () => {
  return (
      <h1>{UserStore.user.name}</h1>
  );
}

export default observer(Header);
