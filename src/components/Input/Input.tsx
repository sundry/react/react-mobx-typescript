import React from 'react';
import UserStore from 'stores/UserStore';

const Input: React.FC = () => {
  return (
    <input onChange={(e: any) => UserStore.setName(e.target.value)} />
  );
}

export default Input;
