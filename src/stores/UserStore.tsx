import { observable, action } from 'mobx';

interface User {
  name: string;
}

class UserStore {
  @observable user: User = { name: ''}
  @action
  setName(name: string) {
    this.user.name = name;
  }
}

export default new UserStore();